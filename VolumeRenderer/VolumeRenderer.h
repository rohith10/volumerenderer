#pragma once

/*
	*	VolumeRenderer.h
	*	
	*	Header file containing declarations for functions used in the Volumetric Renderer
	*
	*	(c) 2012 - 2013 Rohith Chandran.
*/
/*	
	*	VolumeRenderer.cpp - Contains declarations of procedures used in 
	*	the Volumetric Renderer.
	*
	*	(c) 2012-2013 Rohith Chandran.
*/

#include	<iostream>
#include	<fstream>

#include	"VR_datstruct.h"
#include	"EasyBMP.h"
#include	"perlin.h"

typedef	mathVector	Vector;

bool		parseConfig (char inFile [], VRconfig &configStruct, char **filename, voxel **VoxelGrid);
void		setupProjection (const VRconfig &configStruct, projPlane &planeToBeSetup);
void		getIntercepts (const Vector &P, const Vector &E, const VRconfig &configStruct, Vector &interceptStart, Vector &interceptEnd);
int			getSamplingPoints (const Vector &P, const Vector &E, const Vector &start, const Vector &end, float stepSize, Vector **samplingPointsList);
void		computeLightValuesAtVoxel (voxel *VoxelGrid, unsigned int voxID, Vector &pointOnVoxel, const VRconfig &configStruct);
void		buildVoxelGrid (voxel *VoxelGrid, const VRconfig &configStruct);

unsigned long	getVoxelIndex (Vector	&point, const VRconfig &configStruct);