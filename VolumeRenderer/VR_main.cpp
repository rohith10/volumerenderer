/*	
	*	VR_main.cpp - Implements a Volumetric Renderer using Ray March.
	*
	*	(c) 2012-2013 Rohith Chandran.
*/

#include	"VolumeRenderer.h"
#include	<ctime>

using std::ifstream;
using std::ofstream;
using std::cout;
using std::cin;

float kappa;	

int	main (int argc,	char *argv [])
{
	char	*outFile = NULL, *inFile = NULL;
	VRconfig	configSettings;
	projPlane	renderScreen;
	voxel		*VoxelGrid = NULL;

	BMP			outputFile;
	bool		fastRun = false;
	time_t		startTime, renderTime;

	configSettings.lowerLeft [x] = 0;
	configSettings.lowerLeft [y] = 0;
	configSettings.lowerLeft [z] = 0;

	kappa = 1;

	if (argc < 2)
	{
		inFile = new char [strlen ("test1.txt") + 1];
		strcpy (inFile, "test1.txt");
	}
	else
	{
		inFile = new char [strlen (argv [1]) + 1];
		strcpy (inFile, argv [1]);
		
		if (argc > 2)
		{
			if (argv [2])
				kappa = strtod (argv [2], NULL);				

			if (argc > 3)
			{
				if (argv [3])
					if (!strcmp (argv [3], "fastrun"))
						fastRun = true;
				if (argc > 4)
					if (argv [4])
					{
						char *pts = argv [4] + 5;
						int	loopVar = 0;
						while (*pts != '\0')
						{	
							configSettings.lowerLeft [loopVar] = strtod (pts, &pts);
							loopVar ++;
						}
					}
			}
		}
	}

	cout << "\nParsing config file...";
	if (parseConfig (inFile, configSettings, &outFile, &VoxelGrid))
	{
		cout << "\tSuccess!\nBuilding voxel grid...";
		startTime = time (NULL);
		buildVoxelGrid (VoxelGrid, configSettings);
		cout << "\nSetting up projection...";
		outputFile.SetSize (configSettings.resolution [x], configSettings.resolution [y]);
		outputFile.SetBitDepth (24);

		setupProjection (configSettings, renderScreen);
		cout << "\nRendering image...\n";
		renderTime = time (NULL);

		double	xStep, yStep;
		Vector	P, M (renderScreen.perspCentre), E (configSettings.eyePos);
		int	xCoord, yCoord;

		Vector	interceptStart, interceptEnd;
		Vector	*samplingPointsList = NULL;
		int		nSamples = 0;

		float inverseKappa = 1.0f/kappa;

		for (xStep = 0.0, xCoord = 0; xStep <= 1.0; xStep += renderScreen.pixelStepX)
		{
			float twiceXStepMinusOne = (2.0f * xStep) - 1.0f;
			for (yStep = 0.0, yCoord = configSettings.resolution [1] - 1, nSamples = 0; yStep <= 1.0; yStep += renderScreen.pixelStepY)
			{
				P = M + (renderScreen.halfHorizontal*twiceXStepMinusOne) + (renderScreen.halfVertical*(float)((2*yStep) - 1));
				getIntercepts (P, E, configSettings, interceptStart, interceptEnd);

				if ((interceptStart != E) && (interceptEnd != E))
				{
					nSamples = getSamplingPoints (P, E, interceptStart, interceptEnd, configSettings.StepSize, &samplingPointsList);
					if (nSamples < 0)
					{
						cout << "\nERROR: Not Enough Memory.";
						goto	cleanup;
					}
				}
				else if ((interceptStart == E) && (interceptEnd == E))		// Ray doesn't intersect voxel grid.
					nSamples = 0;
				else if (interceptStart == E)
					nSamples = getSamplingPoints (P, E, interceptEnd, interceptEnd, configSettings.StepSize, &samplingPointsList);
				else if (interceptEnd == E)
					nSamples = getSamplingPoints (P, E, interceptStart, interceptStart, configSettings.StepSize, &samplingPointsList);

				float	colourVals [3];
				for (int loopVar = 0; loopVar < 3; ++loopVar)
					colourVals [loopVar] = configSettings.BGColour [loopVar]; 

				if (nSamples > 0)
				{
					for (int loopVar2 = 0; loopVar2 < 3; ++loopVar2)
						colourVals [loopVar2] = 0;
					float	T = 1.0f, constants, delT;		// T stands for transmittence of the view-ray. 
															// Contribution of background colour at a pixel = T,
															// Contribution of voxel densities at a pixel = 1-T.
					for (int loopVar = 0; loopVar < nSamples; ++loopVar)
					{
						unsigned int	voxelIndex = getVoxelIndex (samplingPointsList [loopVar], configSettings);
						if (voxelIndex < 0)
						{
							cout << "\nERROR: Unspecified Error occured while finding Voxel Index.";
							goto cleanup;
						}

						// Not required now that we're pre-computing light values.
//						if (VoxelGrid [voxelIndex].lightVal [0] == -1)
//							computeLightValuesAtVoxel (VoxelGrid, voxelIndex, samplingPointsList [loopVar], configSettings);

						// Calculate transmittence for this sample point along the ray.
						// Transmittence = e ^ (-sd), where s = voxel size and d = its density.
						delT = exp (-(configSettings.StepSize * VoxelGrid [voxelIndex].density));
						T *= delT;			// Accumulate transmittence along the ray.
						constants = ((1.0f-delT)*inverseKappa)*T;
						for (int loopVar2 = 0; loopVar2 < 3; ++loopVar2)
							// Calculate colour at this sampling point and accumulate it along the ray.
							colourVals [loopVar2] += constants*configSettings.BuffColour [loopVar2]*VoxelGrid [voxelIndex].lightVal [loopVar2];
						if (T < 0.01f)
							if (fastRun)
								break;
					}
					float	OneminusT = 1.0f - T;
					for (int loopVar2 = 0; loopVar2 < 3; ++loopVar2)
						// Lerp between accumulated colour along ray and background colour using accummulated transmittence.
						colourVals [loopVar2] = (colourVals [loopVar2] * OneminusT) + (configSettings.BGColour [loopVar2] * T);
				}

				outputFile (xCoord, yCoord)->Red = colourVals [r]*255;
				outputFile (xCoord, yCoord)->Blue = colourVals [b]*255;
				outputFile (xCoord, yCoord)->Green = colourVals [g]*255;

				yCoord --;
				delete [] samplingPointsList;
				samplingPointsList = NULL;
			}
			xCoord ++;
			cout << "\r" << floor (xStep*100) << " percent complete.";
		}
		cout << "\r100 percent complete. Rendered in " << difftime (time (NULL), renderTime) << " seconds."
			 << "\nTotal time: " << difftime (time (NULL), startTime) << " seconds.";
		outputFile.WriteToFile (outFile);
	}
	
	else if (!strcmp (outFile, "NOTFOUND"))
		cout << "\nERROR: Input file not specifed or found.";
	else if (!strcmp (outFile, "MEMERR"))
		cout << "\nERROR: Not Enough Memory.";
	else
		cout << "\nERROR: Config file not in specified format or corrupted.";

cleanup:
	delete [] inFile;
	delete [] outFile;
	if (configSettings.primArray)
		delete [] configSettings.primArray;
	if (VoxelGrid)
		delete [] VoxelGrid;

	cout << "\nPress any key to quit...";
	if (cin.peek () == '\r')
		cin.ignore ();
	cin.get ();

	return	0;
}