/*	
	*	VolumeRenderer.cpp
	*	------------------
	*	Implementation of mathVector class specification in VR_datstruct.h
	*	and procedures declared in VolumeRenderer.h
	*	
	*	(c) 2012 - 2013 Rohith Chandran.
*/

#include	"VolumeRenderer.h"
#include	<ctime>

const	float	pi = 3.1415926;
extern float	kappa;

float	freq, amp;
int		octaves;	

inline	float	max (float a, float b);

// Vector/mathVector class methods definitions:
Vector::mathVector ()
{
	components [x] = 0;
	components [y] = 0;
	components [z] = 0;
}

Vector::mathVector (float x_in, float y_in, float z_in)
{
	components [x] = x_in;
	components [y] = y_in;
	components [z] = z_in;
}

Vector::mathVector (float fromPoint [3], float toPoint [3])
{
	components [x] = toPoint [x] - fromPoint [x];
	components [y] = toPoint [y] - fromPoint [y];
	components [z] = toPoint [z] - fromPoint [z];
}

Vector::mathVector (const float point [3])
{
	components [x] = point [0];
	components [y] = point [1];
	components [z] = point [2];
}

Vector	Vector::operator+ (const Vector &addend)
{
	Vector	sum;
	sum.components [x] = components [x] + addend.components [x];
	sum.components [y] = components [y] + addend.components [y];
	sum.components [z] = components [z] + addend.components [z];
	
	return	sum;
}

Vector	Vector::operator- (const Vector &subtrahend)
{
	Vector	diff;
	diff.components [x] = components [x] - subtrahend.components [x];
	diff.components [y] = components [y] - subtrahend.components [y];
	diff.components [z] = components [z] - subtrahend.components [z];
	
	return	diff;
}

Vector	Vector::operator* (const int multiplier)
{
	Vector	resultant;
	resultant.components [x] = components [x] * multiplier;
	resultant.components [y] = components [y] * multiplier;
	resultant.components [z] = components [z] * multiplier;

	return	resultant;
}

Vector	Vector::operator* (const float multiplier)
{
	Vector	resultant;
	resultant.components [x] = components [x] * multiplier;
	resultant.components [y] = components [y] * multiplier;
	resultant.components [z] = components [z] * multiplier;

	return	resultant;
}

Vector	Vector::operator/ (const int divider)
{
	Vector	resultant;
	resultant.components [x] = components [x] / divider;
	resultant.components [y] = components [y] / divider;
	resultant.components [z] = components [z] / divider;

	return	resultant;
}

Vector	Vector::operator/ (const float divider)
{
	Vector	resultant;
	resultant.components [x] = components [x] / divider;
	resultant.components [y] = components [y] / divider;
	resultant.components [z] = components [z] / divider;

	return	resultant;
}

Vector&	Vector::operator= (const Vector &equivalent)
{
	components [x] = equivalent.components [x];
	components [y] = equivalent.components [y];
	components [z] = equivalent.components [z];

	return *this;
}

Vector&	Vector::operator+= (const Vector &equivalent)
{
	components [x] += equivalent.components [x];
	components [y] += equivalent.components [y];
	components [z] += equivalent.components [z];

	return *this;
}

Vector&	Vector::operator-= (const Vector &equivalent)
{
	components [x] -= equivalent.components [x];
	components [y] -= equivalent.components [y];
	components [z] -= equivalent.components [z];

	return *this;
}

Vector&	Vector::operator*= (const int &scaleFactor)
{
	components [x] *= scaleFactor;
	components [y] *= scaleFactor;
	components [z] *= scaleFactor;

	return *this;
}

Vector&	Vector::operator*= (const float &scaleFactor)
{
	components [x] *= scaleFactor;
	components [y] *= scaleFactor;
	components [z] *= scaleFactor;

	return *this;
}

Vector&	Vector::operator/= (const int &scaleFactor)
{
	components [x] /= scaleFactor;
	components [y] /= scaleFactor;
	components [z] /= scaleFactor;

	return *this;
}

Vector&	Vector::operator/= (const float &scaleFactor)
{
	components [x] /= scaleFactor;
	components [y] /= scaleFactor;
	components [z] /= scaleFactor;

	return *this;
}

bool	Vector::operator== (const Vector &comparable)
{
	if ((components [x] == comparable.components [x]) && (components [y] == comparable.components [y]) &&
		(components [z] == comparable.components [z]))
		return	true;
	else
		return	false;
}

// Dot product of this mathVector object and multiplier.
float	Vector::dot (const mathVector &multiplier)
{
	float	xpdt, ypdt, zpdt;
	xpdt = components [x] * multiplier.components [x];
	ypdt = components [y] * multiplier.components [y];
	zpdt = components [z] * multiplier.components [z];
	xpdt += (ypdt + zpdt);

	return	xpdt;
}

// Cross product.
Vector	Vector::cross (const Vector &multiplicand)
{
	Vector	crossProduct;
	crossProduct.components [x] = (components [y] * multiplicand.components [z]) - (components [z] * multiplicand.components [y]);
	crossProduct.components [y] = (components [z] * multiplicand.components [x]) - (components [x] * multiplicand.components [z]);
	crossProduct.components [z] = (components [x] * multiplicand.components [y]) - (components [y] * multiplicand.components [x]);

	return	crossProduct;
}

// Given a starting point E and an ending point P for a ray, this method will 
// return the intercept value of the point stored in this object along the ray.
// 
// Intercept value = t, where *this = E + ((P-E).normalize) * t
float	Vector::t (Vector E, Vector P)
{
	Vector PEnormalized = (P - E);
	PEnormalized = PEnormalized / PEnormalized.length ();

	Vector currVector = *this;
	currVector = currVector - E;

	return	(currVector [x] / PEnormalized [x]);
}



/*
	Parses the configuration file and stores the appropriate values in configStruct. 
	Also initializes the Voxel Grid with density values in the config file.
	If all values are parsed successfully, it returns true.
	In the event of successful parse, the output file name can be found in outfilename. 
	
	Returns false on either of two conditions: a) the config file is not in proper format   b) there was an error in memory allocation.
	In the first case, outfilename will be set to "NOTFOUND" and in the second, "MEMERR".
*/
bool	parseConfig (char inFile [], VRconfig  &configStruct, char  **outfilename, voxel **VoxelGrid)
{
	char	inString [100];
	char	paramNames [][5] = { "DELT", "STEP", "XYZC", "BRGB",
								 "MRGB", "RESO", "EYEP", "VDIR",
								 "UVEC", "FOVY", "LPOS", "LCOL" };
	char	*valuePtr = NULL;
	int		loopVar = 0, loopVar2 = 0;
	float	configVals [30];

	std::ifstream	fileIn;
	fileIn.open (inFile);

	if (fileIn.is_open ())
	{
		while (loopVar2 != 29)
		{
			fileIn.getline (inString, sizeof (inString));
			if (strstr (inString, paramNames [loopVar]))
			{	// Read all numeric values in order:
				valuePtr = inString + 5;
				while (*valuePtr != '\0')
				{
					if (loopVar2 == 30)
						return	false;
					configVals [loopVar2] = (float) strtod (valuePtr, &valuePtr);
					loopVar2 ++;
				}

				loopVar ++;
			}
			else if ((loopVar == 5) && strstr (inString, "FILE"))
			{	// Read the filename:
				valuePtr = inString + 5;
				*outfilename = new char [strlen (valuePtr) + 1];
				strcpy (*outfilename, valuePtr);
			}
			else
			{	// If an undefined identifier is found, return false to indicate the file is not correct:
				fileIn.close ();
				return	false;
			}
		}

		// Setup the configuration structure with the values read-in.
		configStruct.VoxSize = configVals [0];
		configStruct.StepSize = configVals [1];
		configStruct.fieldOfView [y] = configVals [22];
		configStruct.resolution [x] = (int) configVals [11];
		configStruct.resolution [y] = (int) configVals [12];

		for (loopVar = 0; loopVar < 3; loopVar ++)
		{
			configStruct.BGColour [loopVar] = configVals [5 + loopVar];
			configStruct.BuffColour [loopVar] = configVals [8 + loopVar];
			configStruct.eyePos [loopVar] = configVals [13 + loopVar];
			configStruct.lightColour [loopVar] = configVals [26 + loopVar];
			configStruct.lightPos [loopVar] = configVals [23 + loopVar];
			configStruct.viewDir [loopVar] = configVals [16 + loopVar];
			configStruct.upVector [loopVar] = configVals [19 + loopVar];
			configStruct.VoxGridBound [loopVar] = (int) configVals [2 + loopVar];
		}

		float	aspectRatio = (float) configStruct.resolution [x] / configStruct.resolution [y];
		configStruct.fieldOfView [x] = atan (aspectRatio * tan ((configStruct.fieldOfView [y] * pi) / 180));

		unsigned long	nVoxels = configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] * configStruct.VoxGridBound [z];
		*VoxelGrid = new voxel [nVoxels];
		if (!*VoxelGrid)
		{	// Out of memory. Return false.
			*outfilename = new char [strlen ("MEMERR") + 1];
			strcpy (*outfilename, "MEMERR");
			return	false;
		}

		for (int loopVar = 0; loopVar < nVoxels; loopVar ++)
		{
			(*VoxelGrid + loopVar)->lightVal [0] = -1;
			(*VoxelGrid + loopVar)->density = 0; 
		}

		// Now read in the actual primitives.
		// First, read the number of primitives contained in the file. 
		do
		{
			fileIn.getline (inString, sizeof (inString));
		} while (strlen (inString) == 0);	// Skip empty lines.
		configStruct.nPrimitives = (int) strtod (inString, NULL);
		configStruct.primArray = new Primitive [configStruct.nPrimitives];

		int elementCount;
		for (elementCount = 0; !fileIn.eof (); elementCount ++)
		{
			do
			{
				fileIn.getline (inString, sizeof (inString));
			} while (strlen (inString) == 0);	// Skip empty lines.

			// Compare primitive name in file and set primitive accordingly.
			if (strcmp (inString, "sphere") == 0)
				configStruct.primArray [elementCount].Type = balls;
			else if (strcmp (inString, "cloud") == 0)
				configStruct.primArray [elementCount].Type = puff;
			else	// pyroclastic
				configStruct.primArray [elementCount].Type = poof;

			// Read in primitive centre point... 
			fileIn.getline (inString, sizeof (inString));
			valuePtr = inString;
			int xyz = 0;
			while (*valuePtr != '\0')
			{
				configStruct.primArray [elementCount].centre [xyz] = (float) strtod (valuePtr, &valuePtr);
				xyz ++;
			}

			// ... and radius.
			fileIn.getline (inString, sizeof (inString));
			configStruct.primArray [elementCount].radius = (float) strtod (inString, NULL);
			configStruct.primArray [elementCount].inverseRadius = 1/configStruct.primArray [elementCount].radius; 
		}
		if (elementCount < configStruct.nPrimitives)
		{	// If the number of primitives read < primitive count specified, return false as file is not correct.
			fileIn.close ();
			return	false;
		}
	}

	else
	{	// Input file was not found. Return false.
		*outfilename = new char [strlen ("NOTFOUND") + 1];
		strcpy (*outfilename, "NOTFOUND");
		return	false;
	}

	fileIn.close ();

	freq = 0.3; amp = 0.6; octaves = 1;
	fileIn.open ("perlin_configs.txt");
	if (fileIn.is_open ())
	{
		fileIn.getline (inString, sizeof (inString));
		valuePtr = inString;
		loopVar2 = 0;
		while (*valuePtr != '\0')
		{
			if (loopVar2 == 30)
				break;
			configVals [loopVar2] = (float) strtod (valuePtr, &valuePtr);
			loopVar2 ++;
		}
		
		if (loopVar2 == 3)
		{
			octaves = (int) configVals [0];
			freq = configVals [1];
			amp = configVals [2];
		}
	}
	fileIn.close ();
	return	true;
}

/*	
	Sets up the projection plane, planeToBeSetup.
	The projection plane has members perspCentre - a float array storing co-ordinates, halfHorizontal (the vector H) and halfVertical (the vector V).
*/
void	setupProjection (const	VRconfig &configStruct, projPlane &planeToBeSetup)
{
	planeToBeSetup.perspCentre [x] = configStruct.eyePos [x] + configStruct.viewDir [x];
	planeToBeSetup.perspCentre [y] = configStruct.eyePos [y] + configStruct.viewDir [y];
	planeToBeSetup.perspCentre [z] = configStruct.eyePos [z] + configStruct.viewDir [z];

	Vector	A, B, C, Up (configStruct.upVector);
	Vector	Eye (configStruct.eyePos), ProjectionCentre (planeToBeSetup.perspCentre);
	
	C = ProjectionCentre - Eye;
	A = C.cross (Up);
	B = A.cross (C);
	planeToBeSetup.halfHorizontal = (A*C.length ()*tan (configStruct.fieldOfView [x]))/A.length ();
	planeToBeSetup.halfVertical = (B*C.length ()*tan ((configStruct.fieldOfView [y] * pi) / 180))/B.length ();
	planeToBeSetup.pixelStepX = (double) 1 / (configStruct.resolution [x] - 1);
	planeToBeSetup.pixelStepY = (double) 1 / (configStruct.resolution [y] - 1);
}

/*	LEGACY CODE
	For a given vector from E to P, this function finds the intercept it makes with the Voxel Grid.
	The intercept's start is marked by the point interceptStart and its end is marked by interceptEnd.
	If it does not intersect the Voxel Grid, interceptStart and interceptEnd are both set to E.
	If it only intersects the Grid at one point, only interceptStart will be valid.

void	getIntercepts (const Vector &P, const Vector &E, const VRconfig &configStruct, 
						Vector &interceptStart, Vector &interceptEnd)
{
	// This intersection function intersects a ray with each of the 6 planes of the voxel grid, finds the 
	// intersection points and puts the one closest to E in interceptStart and the other one in interceptEnd.

	// Define the upper and lower bounds of the voxel grid.
	Vector  lowerLeftFront (configStruct.lowerLeft), 
			upperRightBack (configStruct.lowerLeft [x] + (configStruct.VoxSize*configStruct.VoxGridBound [x]), 
							configStruct.lowerLeft [y] + (configStruct.VoxSize*configStruct.VoxGridBound [y]),
							configStruct.lowerLeft [z] - (configStruct.VoxSize*configStruct.VoxGridBound [z]));
	
	// Define the three normals. Since our voxel grid is aligned to the co-ordinate axes, we use them directly.  
	Vector	i (1.0, 0.0, 0.0), j (0.0, 1.0, 0.0), k (0.0, 0.0, 1.0);

	Vector	corners [2], unitVectors [3];
	corners [0] = lowerLeftFront;
	corners [1] = upperRightBack;

	// We start with the planes that face front, down and right.
	unitVectors [0] = k;
	unitVectors [1] = j*-1;
	unitVectors [2] = i*-1;

	float	num [6];
	float	den [6];
	float	t [6];

	Vector	PEnormalized = P;
	PEnormalized = (PEnormalized-E)/(PEnormalized-E).length ();

	corners [0] = corners [0] - E;
	corners [1] = corners [1] - E;

	for (int loopVar = 0, signChanger = 1; loopVar < 6; loopVar ++)
	{
		if (loopVar == 3)
			// After we intersect it with 3 planes, switch the direction of the normals to intersect it with the other three.
			signChanger *= -1;
		num [loopVar] = corners [loopVar/3].dot (unitVectors [loopVar%3]*signChanger);
		den [loopVar] = (PEnormalized.dot (unitVectors [loopVar%3]*signChanger));
		if (den [loopVar] == 0)
		{ 
			if (num [loopVar] == 0)
			{	// If numerator and denominator are both zero, then the vector lies on the current plane.
				t [loopVar] = 0;
			}
			else
				// Otherwise, there's no intersection. So, set to invalid.
				t [loopVar] = -32767;
		}

		else
			t [loopVar] = num [loopVar] / den [loopVar];
	}

	// Run through the set of intercepts and find interceptStart, followed by interceptEnd.
	for (int loopVar = 0; loopVar < 6; loopVar ++)
	{
		if (t [loopVar] == -32767)
			if (num [loopVar] == 0)
			{
				interceptStart = E;
				continue;
			}
							
		interceptStart = E;
		interceptStart = interceptStart + (PEnormalized * t [loopVar]);
		if (((interceptStart [x] >= (lowerLeftFront [x]-0.0001)) && (interceptStart [x] <= (upperRightBack [x]+0.0001))) &&
			((interceptStart [y] >= (lowerLeftFront [y]-0.0001)) && (interceptStart [y] <= (upperRightBack [y]+0.0001))) &&
			((interceptStart [z] <= (lowerLeftFront [z]+0.0001)) && (interceptStart [z] >= (upperRightBack [z]-0.0001))))
			break;
		else
			interceptStart = E;
	}

	for (int loopVar = 0; loopVar < 6; loopVar ++)
	{
		if (t [loopVar] == -32767)
			if (num [loopVar] == 0)
			{
				interceptEnd = E;
				continue;
			}
						
		interceptEnd = E;
		interceptEnd = interceptEnd + (PEnormalized * t [loopVar]);
		if (((interceptEnd [x] >= (lowerLeftFront [x]-0.0001)) && (interceptEnd [x] <= (upperRightBack [x]+0.0001))) &&
			((interceptEnd [y] >= (lowerLeftFront [y]-0.0001)) && (interceptEnd [y] <= (upperRightBack [y]+0.0001))) &&
			((interceptEnd [z] <= (lowerLeftFront [z]+0.0001)) && (interceptEnd [z] >= (upperRightBack [z]-0.0001))))
			if (interceptEnd == interceptStart)
				continue;
			else
				break;
		else
			interceptEnd = E;					
	}
} */

/*	
	For a given vector from E to P, this function finds the intercept it makes with the Voxel Grid.
	The intercept's start is marked by the point interceptStart and its end is marked by interceptEnd.
	If it does not intersect the Voxel Grid, interceptStart and interceptEnd are both set to E.
	If it only intersects the Grid at one point, only interceptStart will be valid.
*/

void	getIntercepts (const Vector &P, const Vector &E, const VRconfig &configStruct, 
						Vector &interceptStart, Vector &interceptEnd)
{
	// Uses the slab method to check for intersection.
	// Refer http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter3.htm for details.

	// Define the constants. tnear = -INFINITY ; tfar = +INFINITY (+/- 1e6 for practical purposes)
	float tnear = -1e6, tfar = 1e6;
	float epsilon = 1e-3;

	// Extremities.
	Vector  lowerLeftFront (configStruct.lowerLeft), 
			upperRightBack (configStruct.lowerLeft [x] + (configStruct.VoxSize*configStruct.VoxGridBound [x]), 
							configStruct.lowerLeft [y] + (configStruct.VoxSize*configStruct.VoxGridBound [y]),
							configStruct.lowerLeft [z] - (configStruct.VoxSize*configStruct.VoxGridBound [z]));
	Vector rayDir = P;
	rayDir -= E;
	rayDir /= rayDir.length ();

	Vector rayOrig = E;

	// For each X, Y and Z, check for intersections using the slab method as described above.
	for (int loopVar = 0; loopVar < 3; loopVar ++)
	{
		if (fabs (rayDir [loopVar]) < epsilon)
		{
			if ((rayOrig [loopVar] < lowerLeftFront [loopVar]-epsilon) || (rayOrig [loopVar] > upperRightBack [loopVar]+epsilon))
			{	
				tnear = -1.0f;		// No Intersection. Set both tnear and tfar to -1.
				tfar = -1.0f;
				break;
			}
		}
		else
		{
			float t1 = (lowerLeftFront [loopVar] - rayOrig [loopVar]) / rayDir [loopVar];
			float t2 = (upperRightBack [loopVar] - rayOrig [loopVar]) / rayDir [loopVar];

			if (t1 > t2+epsilon)
			{
				t2 += t1;
				t1 = t2 - t1;
				t2 -= t1;
			}

			if (tnear < t1-epsilon)
				tnear = t1;

			if (tfar > t2-epsilon)
				tfar = t2;

			if (tnear > tfar+epsilon)
			{	
				tnear = -1.0f;		// No Intersection. Set both tnear and tfar to -1.
				tfar = -1.0f;
				break;
			}

			if (tfar < 0.0f-epsilon)
			{	
				tnear = -1.0f;		// No Intersection. Set tnear to -1 (tfar already is).
				break;
			}
		}
	}
	
	if (tfar < 0.0f-epsilon)
	{
		interceptStart = E;
		interceptEnd = E;
	}
	else
	{
		if (tnear < 0.0f-epsilon)
		{
			interceptStart = rayOrig;
			interceptEnd = rayOrig + (rayDir * tfar);
		}
		else
		{
			interceptStart = rayOrig + (rayDir * tnear);
			interceptEnd = rayOrig + (rayDir * tfar);
		}
	}
}

/*
	For a given vector from eyePosition to pointOnProjectionPlane, starting and ending points of the ray march (start and end respectively)
	and a specified step size (stepSize), this creates an array of all sampling points from start to end along the specified vector, 
	spaced stepSize distance apart. Returns the count of the no. of sampling points.

	When start = end, it will return just one point - the end point.

	If memory cannot be allocated for the array, it returns -1.
*/
int		getSamplingPoints (const Vector &pointOnProjectionPlane, const Vector &eyePosition, const Vector &start, 
							const Vector &end, float stepSize, Vector **samplingPointsList)
{
	Vector	P = pointOnProjectionPlane;
	Vector	E = eyePosition;
	Vector	loopEnd = end;
	Vector	PEStepSize = (P-E)/(P-E).length();
	PEStepSize*=stepSize;

	// Count the number of samples required.
	int	count = 0;
	Vector tmp = loopEnd - start;
	float nSamplesF = (tmp.length () / stepSize);
	count = nSamplesF; 
	// We need to account for the start point, which should also be counted.	
	if (modf (nSamplesF, &nSamplesF) > 0.01f)
		count += 2;			// If the fractional part is greater than 0.01, then the end point is not included (counted) in count.
							// We need to account for that.
	else
		count += 1;

	// Create the array of sample points.
	*samplingPointsList = new Vector [count];
	if (!*samplingPointsList)
		return	-1;

	Vector loopV = start;
 	for (int loopVar = 0; loopVar < count; ++loopVar)
	{
		*(*samplingPointsList + loopVar) = loopV;
		loopV += PEStepSize;
	}
	//*(*samplingPointsList + count2) = end;//
	return	count;
}

/*
	Given a point within the Voxel Grid, this function returns the index of the voxel in the Voxel Grid that holds it. 
*/
unsigned long	getVoxelIndex (Vector &point, const VRconfig &configStruct)
{
	unsigned long	voxID = 0;
	Vector	p2 = point;

	// Check bounds.
	// Tolerance limit is 1 voxel. A point within boundary +/- 1 voxel resolves to boundary voxel.  
	// Upper bound:
	if ((point [x] >= (configStruct.VoxGridBound [x]+1)) ||
		(point  [y] >= (configStruct.VoxGridBound [y]+1)) ||
		(point  [z] <= (-configStruct.VoxGridBound [z]-1)))
		return -1;

	// Lower bound:
	if ((point [x] <= (configStruct.lowerLeft [x]-1)) ||
		(point [y] <= (configStruct.lowerLeft [y]-1)) ||
		(point [z] >= (configStruct.lowerLeft [z]+1)))
		return -1;

	// Enforce tolerance limits:
	if (point [x] >= configStruct.VoxGridBound [x])
		p2 [x] = configStruct.VoxGridBound [x] - 1;
	else if (point [x] < configStruct.lowerLeft [x])
		p2 [x] = configStruct.lowerLeft [x];

	if (point [y] >= configStruct.VoxGridBound [y])
		p2 [y] = configStruct.VoxGridBound [y] - 1;
	else if (point [y] < configStruct.lowerLeft [y])
		p2 [y] = configStruct.lowerLeft [y];
	
	if (abs (point [z]) >= configStruct.VoxGridBound [z])
		p2 [z] = -configStruct.VoxGridBound [z] + 1;
	else if (point [z] > configStruct.lowerLeft [z])
		p2 [z] = configStruct.lowerLeft [z];

	// Compute Voxel ID:
	voxID = ((floor (abs(p2 [z])) - configStruct.lowerLeft [z]) * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y]) +
			((floor (p2 [y]) - configStruct.lowerLeft [y]) * configStruct.VoxGridBound [x]) +
			(floor (p2 [x]) - configStruct.lowerLeft [x]);

	return	voxID;
}

/*
	Computes (and stores) the RGB values of light at a specified voxel (given by VoxelGrid [voxID]).
*/
void		computeLightValuesAtVoxel (voxel *VoxelGrid, unsigned int voxID, Vector &samplePoint, const	VRconfig &configStruct)
{
	Vector	lightPosition (configStruct.lightPos);
	Vector	interceptStart, interceptEnd;
	// Get the intercept that a vector from lightPosition to samplePoint makes with the voxel grid.
	getIntercepts (samplePoint, lightPosition, configStruct, interceptStart, interceptEnd);

	int		nSamples = 0;
	Vector	*samplingPointsList = NULL;

	// Initialize light value at voxel to light's colour.
	VoxelGrid [voxID].lightVal [r] = configStruct.lightColour [r];
	VoxelGrid [voxID].lightVal [g] = configStruct.lightColour [g];
	VoxelGrid [voxID].lightVal [b] = configStruct.lightColour [b];

	// Find the intercept (t) value of intercept start and end points along the vector from lightPosition to samplePoint.
	float	t2 = interceptStart.t (samplePoint, lightPosition);
	float	t1 = interceptEnd.t (samplePoint, lightPosition);

	if (t2 < 0)
		interceptStart = interceptEnd;

	if (interceptStart != samplePoint)
		nSamples = getSamplingPoints (samplePoint, lightPosition, interceptStart, samplePoint, configStruct.StepSize, &samplingPointsList);
	else
		nSamples = 0;
	if (nSamples > 0)
	{
		float	Q = 0;
		for (int loopVar = 0; loopVar < nSamples; ++loopVar)
		{	// Step through the number of samples and compute light's transmittence (Q) at every point.
			unsigned int	voxelIndex = getVoxelIndex (samplingPointsList [loopVar], configStruct);
			if (voxelIndex < 0)
				break;			// When an access to a non-existent (out of bounds) voxel is detected, stop computing Q.
			Q += configStruct.StepSize * VoxelGrid [voxelIndex].density;
		}
		Q = exp (-kappa*Q);
		
		for (int loopVar2 = 0; loopVar2 < 3; ++loopVar2)
			VoxelGrid [voxID].lightVal [loopVar2] *= Q;
	}

	if (samplingPointsList)
		delete [] samplingPointsList;
}

/*
	"Builds" the voxel grid - i.e. computes the densities and light vals of all the voxels - based on the primitives
	specified in the sample file provided as input and the parameters in "perlin_config.txt".
*/
void		buildVoxelGrid (voxel *VoxelGrid, const VRconfig &configStruct)
{
	Perlin noise (octaves, freq, amp, (time(NULL)%1000));

	float	centrePointPos = (configStruct.VoxSize / 2.0);
	Vector	centrePoint;
	float	distance;

	// Calculate densities at voxel centres.
	for (float loopVarZ = configStruct.lowerLeft [z] - centrePointPos; loopVarZ > -configStruct.VoxGridBound [z]; --loopVarZ)
		for (float loopVarY = configStruct.lowerLeft [y] + centrePointPos; loopVarY < configStruct.VoxGridBound [y]; ++loopVarY)
			for (float loopVarX = configStruct.lowerLeft [x] + centrePointPos; loopVarX < configStruct.VoxGridBound [x]; ++loopVarX)
			{
				//centrePoint.setcomponent (x, loopVarX);
				//centrePoint.setcomponent (y, loopVarY);
				//centrePoint.setcomponent (z, loopVarZ);
				centrePoint = Vector (loopVarX, loopVarY, loopVarZ);

				unsigned int voxID = getVoxelIndex (centrePoint, configStruct);

				for (int loopVar = 0; loopVar < configStruct.nPrimitives; loopVar ++)
				{
					distance = sqrt (pow ((loopVarX - configStruct.primArray [loopVar].centre [x]), 2) +
										 pow ((loopVarY - configStruct.primArray [loopVar].centre [y]), 2) +
										 pow ((abs (loopVarZ) - configStruct.primArray [loopVar].centre [z]), 2));
					if (configStruct.primArray [loopVar].Type == balls)			// Spheres
						VoxelGrid [voxID].density += max ((1.0f - distance*configStruct.primArray [loopVar].inverseRadius), 0.0f);
					else if (configStruct.primArray [loopVar].Type == puff)		// Clouds
						VoxelGrid [voxID].density += 
							(max ((noise.Get (loopVarX, loopVarY, loopVarZ) + (1.0f - distance*configStruct.primArray [loopVar].inverseRadius)),
								    0.0f));
					else														// Pyroclastics
						VoxelGrid [voxID].density += max ((configStruct.primArray [loopVar].radius - (distance*configStruct.primArray [loopVar].inverseRadius)
															 + abs(noise.Get (loopVarX, loopVarY, loopVarZ))) , 0.0f);

					// Make sure that density is between 0.0 and 1.0.
					if (VoxelGrid [voxID].density > 1.0f)
						VoxelGrid [voxID].density = 1.0f;
		//			else if (VoxelGrid [voxID].density < 0)
		//				VoxelGrid [voxID].density = 0;
				}
			}

	// Light val pre-computation for higher efficiency.
	int steps = 5;
	bool overshootZOnce = true;
	// Compute light vals at voxels spaced "steps" apart in each direction. 
	for (float loopVarZ = configStruct.lowerLeft [z]; ; loopVarZ += steps)
	{
		// We need to overshoot once in each direction to calculate the values at the end point voxels properly.
		if ((loopVarZ >= configStruct.VoxGridBound [z])&& overshootZOnce)	// Making sure we don't overshoot the
		{																	// bound in the Z direction more than once.
			loopVarZ = configStruct.VoxGridBound [z] - 1;
			overshootZOnce = false;
		}
		else if (!overshootZOnce)
			break;

		bool overshootYOnce = true;
		for (float loopVarY = configStruct.lowerLeft [y]; ; loopVarY+=steps)
		{	
			if ((loopVarY >= configStruct.VoxGridBound [y])&& overshootYOnce)	// Making sure we don't overshoot the
			{																	// bound in the Y direction more than once.
				loopVarY = configStruct.VoxGridBound [y] - 1;
				overshootYOnce = false;
			}
			else if (!overshootYOnce)
				break;

			bool overshootXOnce = true;
			for (float loopVarX = configStruct.lowerLeft [x]; ; loopVarX+=steps)
			{
				if ((loopVarX >= configStruct.VoxGridBound [x])&& overshootXOnce)	// Making sure we don't overshoot the
				{																	// bound in the X direction more than once.
					loopVarX = configStruct.VoxGridBound [x] - 1;
					overshootXOnce = false;
				}
				else if (!overshootXOnce)
					break;

				float sizeBy2 = (configStruct.VoxSize / 2.0);
				Vector centrePoint (loopVarX+sizeBy2, loopVarY+sizeBy2, -loopVarZ-sizeBy2);
				unsigned int voxelIndex = loopVarZ * configStruct.VoxGridBound [y] * configStruct.VoxGridBound [x] + 
										loopVarY * configStruct.VoxGridBound [x] + loopVarX;
				if (VoxelGrid [voxelIndex].lightVal [0] == -1)
					computeLightValuesAtVoxel (VoxelGrid, voxelIndex, centrePoint, configStruct);
			}
		}
	}

	// Interpolate trilinearly to all other voxels.
	// -----------------------------------------------------------------------
	// We process the voxel grid in blocks of "steps" voxels in each direction. 
	// The corner of each block is, therefore, marked by voxels for which we have already computed lightVal.
	// While processing each block, we tri-lerp the lightVal for all the voxels within the block for which it hadn't been computed yet.
	// Thus, this whole process is about filling in the blanks between the voxels for which lightVal has been computed.
	for (unsigned int loopVarZ = configStruct.lowerLeft [z]; loopVarZ < configStruct.VoxGridBound [z]; loopVarZ+=steps)
		for (unsigned int loopVarY = configStruct.lowerLeft [y]; loopVarY < configStruct.VoxGridBound [y]; loopVarY+=steps)
			for (unsigned int loopVarX = configStruct.lowerLeft [x]; loopVarX < configStruct.VoxGridBound [x]; loopVarX+=steps)
			{	
				//	Stored as follows:  interpVertexIDs = { A, B, D, C, F, G, E, H }
				//  The peculiar form of storage is to easily allow trilerping using a loop.
				//		E-----------H
				//	  / |         /	|
				//	F-----------G	|
				//	|	|		|	|
				//	|	D-------|---C
				//	| /			| /	
				//	A-----------B
				//  = voxels for which the lightVal has been already computed. The voxel IDs are stored.
				unsigned int interpVertexIDs [8];

				// lowerLeftIndex denotes the indices of A and nextSampledVoxelIndex denotes indices of H.
				// From these, we can compute the indices (and thus, voxel IDs) of B, C, D, E, F and G. 
				unsigned int lowerLeftIndex [3] = { loopVarX, loopVarY, loopVarZ };
				unsigned int nextSampledVoxelIndex [3];
				if (loopVarX+steps < configStruct.VoxGridBound [x])
					nextSampledVoxelIndex [x] = loopVarX+steps;
				else
					nextSampledVoxelIndex [x] = configStruct.VoxGridBound [x]-1;

				if (loopVarY+steps < configStruct.VoxGridBound [y])
					nextSampledVoxelIndex [y] = loopVarY+steps;
				else
					nextSampledVoxelIndex [y] = configStruct.VoxGridBound [y]-1;

				if (loopVarZ+steps < configStruct.VoxGridBound [z])
					nextSampledVoxelIndex [z] = loopVarZ+steps;
				else
					nextSampledVoxelIndex [z] = configStruct.VoxGridBound [z]-1;

				unsigned int voxID = 0;
				// Fill in the interpVertexID array with voxel IDs
				// Messy, but using a loop would be messier.				
				// A
				voxID = lowerLeftIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						lowerLeftIndex [y] * configStruct.VoxGridBound [x] + 
						lowerLeftIndex [x];
				interpVertexIDs [0] = voxID;
				// B
				voxID = lowerLeftIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						lowerLeftIndex [y] * configStruct.VoxGridBound [x] + 
						nextSampledVoxelIndex [x];
				interpVertexIDs [1] = voxID;
				// C
				voxID = nextSampledVoxelIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						lowerLeftIndex [y] * configStruct.VoxGridBound [x] + 
						nextSampledVoxelIndex [x];
				interpVertexIDs [3] = voxID;
				// D
				voxID = nextSampledVoxelIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						lowerLeftIndex [y] * configStruct.VoxGridBound [x] + 
						lowerLeftIndex [x];
				interpVertexIDs [2] = voxID;
				// E
				voxID = nextSampledVoxelIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						nextSampledVoxelIndex [y] * configStruct.VoxGridBound [x] + 
						lowerLeftIndex [x];
				interpVertexIDs [6] = voxID;
				// F
				voxID = lowerLeftIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						nextSampledVoxelIndex [y] * configStruct.VoxGridBound [x] + 
						lowerLeftIndex [x];
				interpVertexIDs [4] = voxID;
				// G
				voxID = lowerLeftIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						nextSampledVoxelIndex [y] * configStruct.VoxGridBound [x] + 
						nextSampledVoxelIndex [x];
				interpVertexIDs [5] = voxID;
				// H
				voxID = nextSampledVoxelIndex [z] * configStruct.VoxGridBound [x] * configStruct.VoxGridBound [y] + 
						nextSampledVoxelIndex [y] * configStruct.VoxGridBound [x] + 
						nextSampledVoxelIndex [x];
				interpVertexIDs [7] = voxID;

				float xDenominator = 1.0f / (float) (nextSampledVoxelIndex [x] - lowerLeftIndex [x]);
				float yDenominator = 1.0f / (float) (nextSampledVoxelIndex [y] - lowerLeftIndex [y]);
				float zDenominator = 1.0f / (float) (nextSampledVoxelIndex [z] - lowerLeftIndex [z]);

				// Now, loop over all the voxels within the block. 
				for (unsigned int loopVarZ2 = lowerLeftIndex [z]; loopVarZ2 <= nextSampledVoxelIndex [z]; ++loopVarZ2)
					for (unsigned int loopVarY2 = lowerLeftIndex [y]; loopVarY2 <= nextSampledVoxelIndex [y]; ++loopVarY2)
						for (unsigned int loopVarX2 = lowerLeftIndex [x]; loopVarX2 <= nextSampledVoxelIndex [x]; ++loopVarX2)
						{
							// Trilerp and compute lightVal.
							voxID = loopVarZ2 * configStruct.VoxGridBound [y] * configStruct.VoxGridBound [x] + 
										loopVarY2 * configStruct.VoxGridBound [x] + loopVarX2;
							
							// If voxel already has lightVal computed, skip this iteration and jump to next.
							if (VoxelGrid [voxID].lightVal [0] != -1)
								continue;

							float xRatio = (loopVarX2 - lowerLeftIndex [x]) * xDenominator;
							float yRatio = (loopVarY2 - lowerLeftIndex [y]) * yDenominator;
							float zRatio = (loopVarZ2 - lowerLeftIndex [z]) * zDenominator;

							float oneMinusXRatio = 1 - xRatio;
							float oneMinusYRatio = 1 - yRatio;
							// interpolatedXVals [0] stores x-interpolated lightVal between A and B
							// interpolatedXVals [1] stores x-interpolated lightVal between F and G
							Vector interpolatedXVals [4];
							Vector interpolatedYVals [2];
							Vector interpolatedLightVal;
							for (int i = 0; i < 2; i++)
							{
								interpolatedXVals [2*i] = Vector(VoxelGrid [interpVertexIDs [2*i + 1]].lightVal)* xRatio + 
															Vector(VoxelGrid [interpVertexIDs [2*i]].lightVal) * oneMinusXRatio; 
								interpolatedXVals [2*i + 1] = Vector(VoxelGrid [interpVertexIDs [2*i + 5]].lightVal) * xRatio + 
																Vector(VoxelGrid [interpVertexIDs [2*i + 4]].lightVal) * oneMinusXRatio;
								
								interpolatedYVals [i] = interpolatedXVals [2*i + 1] * yRatio + 
														interpolatedXVals [2*i] * oneMinusYRatio;
							}
							interpolatedLightVal = interpolatedYVals [1]*zRatio + interpolatedYVals [0]*(1-zRatio);
							
							VoxelGrid [voxID].lightVal [0] = interpolatedLightVal [0];
							VoxelGrid [voxID].lightVal [1] = interpolatedLightVal [1];
							VoxelGrid [voxID].lightVal [2] = interpolatedLightVal [2];
						}
			}

//	Validate that light values for all voxels have been computed.
//	std::ofstream fileOut ("errlog.txt");
	bool lightValsNotComputed = false;
	unsigned int loopVarZ2, loopVarY2, loopVarX2;
	for (loopVarZ2 = configStruct.lowerLeft [z]; loopVarZ2 < configStruct.VoxGridBound [z]; ++loopVarZ2)
		for (loopVarY2 = configStruct.lowerLeft [y]; loopVarY2 < configStruct.VoxGridBound [y]; ++loopVarY2)
			for (loopVarX2 = configStruct.lowerLeft [x]; loopVarX2 < configStruct.VoxGridBound [x]; ++loopVarX2)
			{
				unsigned int voxID = loopVarZ2 * configStruct.VoxGridBound [y] * configStruct.VoxGridBound [x] + 
										loopVarY2 * configStruct.VoxGridBound [x] + loopVarX2;
							
				// If voxel already has lightVal computed, skip this iteration and jump to next.
				if (VoxelGrid [voxID].lightVal [0] == -1)
				{
					lightValsNotComputed = true;
//					fileOut << "\n\tHALP! " << loopVarZ2 << "\t" << loopVarY2 << "\t" << loopVarX2;
				}
			}
	if (!lightValsNotComputed)
		std::cout << "\tDone!";
	else
		std::cout << "\tNot Done!";
//	fileOut.close ();

}

inline	float max (float a, float b)
{
	return	(a > b)? a: b;
}