#pragma once

/*
	*	VR_DatStruct.h
	*	
	*	Header file containing declarations for data structures used in the Volumetric Renderer
	*
	*	(c) 2012 - 2013 Rohith Chandran.
*/

enum	{x = 0, r = 0, y = 1, g = 1, z = 2, b = 2};
enum	primtypes {balls, puff, poof};

class	mathVector
{
	float	components [3];

public:
	mathVector ();
	mathVector (float xcomp, float ycomp, float zcomp);
	mathVector (float fromPoint [3], float	toPoint [3]);
	mathVector (const float point [3]);


	mathVector	operator *	(const float multiplier);
	mathVector	operator *	(const int multiplier);
	mathVector	operator /	(const float divider);
	mathVector	operator /	(const int divider);
	mathVector	operator +	(const mathVector &addend);
	mathVector	operator -	(const mathVector &subtrahend);
	mathVector&	operator =  (const mathVector &equivalent);

	mathVector&	operator+= (const mathVector &equivalent);
	mathVector&	operator-= (const mathVector &equivalent);
	mathVector&	operator*= (const int &scaleFactor);
	mathVector&	operator*= (const float &scaleFactor);
	mathVector&	operator/= (const int &scaleFactor);
	mathVector&	operator/= (const float &scaleFactor);

	bool		operator == (const mathVector &comparable);
	bool		operator != (const mathVector &comparable) { return !(*this == comparable); }

	float&		operator [] (int compIndex)	{ return components [compIndex]; }

	float		dot (const mathVector &multiplier);
	mathVector	cross (const mathVector &multiplier);
	
	float		component (int	compIndex)	{ return	components [compIndex]; }
	float		length () { return	(sqrt (pow (components [x], 2) + pow (components [y], 2) + pow (components [z], 2))); }

	void		setcomponent (int compIndex, int value) { components [compIndex] = value; }
	float		t (mathVector E, mathVector	P);
};

struct	Primitive
{
	primtypes Type;
	float	centre [3];
	float	radius;
	float	inverseRadius;
};

struct	VRconfig
{
	float	VoxSize;
	float	StepSize;
	float	BGColour [3];
	float	BuffColour [3];
	float	lightColour [3];

	float	eyePos [3];
	float	lightPos [3];
	float	fieldOfView [2];

	int		VoxGridBound [3];
	int		resolution [2];
	int		nPrimitives;

	float	viewDir [3];
	float	upVector [3];

	float	lowerLeft [3];

	Primitive	*primArray;
};

struct	projPlane
{
	mathVector		halfHorizontal;
	mathVector		halfVertical;

	float		perspCentre [3];
	double		pixelStepX;
	double		pixelStepY;
};

struct	voxel
{
	float	lightVal [3];
	float	density;
};